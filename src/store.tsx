import create from "zustand";

export interface pokemons {
  id: number;
  name: string;
}

type State = {
  pokemons: pokemons[],
  addPokemon: (pokemon: pokemons) => void;
  removePokemon: (id: number) => void;
}

const useStore = create<State>((set) => ({
  pokemons: [],
  addPokemon: (pokemon: pokemons) => {
    set((state) => ({
      pokemons: [
        {id: Math.random() * 100, name: pokemon.name},
        ...state.pokemons
      ]
    }))
  },
  removePokemon: (id: number) => {
    set((state) => ({
      pokemons: state.pokemons.filter((pokemon) => pokemon.id !== id)
    }))
  }
}));

export default useStore;